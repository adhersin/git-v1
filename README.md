# git-v1

## Desctription

Repo de la formation git v1 : présentation + illustration du concept de remote (clone, pull).

Pour aller plus loin : lire la [documentation de git](https://git-scm.com/docs).


Cible d'origine : présentation de Picasoft disponible [ici](https://gitlab.utc.fr/picasoft/formations/A18/git-v1)

## Présentation

La présentation au format PDF est disponible [ici](https://gitlab.utc.fr/adhersin/git-v1/-/jobs/artifacts/master/raw/presentation.pdf?job=beamer-build)
